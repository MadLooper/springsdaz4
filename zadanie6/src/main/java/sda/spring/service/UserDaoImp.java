package sda.spring.service;

import org.springframework.util.StringUtils;
import sda.spring.model.LoginField;
import sda.spring.model.UserField;

import java.util.HashMap;

public class UserDaoImp implements UserDo {

    private HashMap<String, UserField> users = new HashMap<>();

    @Override
    public String register(UserField userField) {
        StringBuilder error = new StringBuilder();
        if (StringUtils.isEmpty(userField.getName())) {
            error.append("field name's missing");
        }
        if (StringUtils.isEmpty(userField.getSurname())) {
            error.append("field surname's missing.");
        }
        if (StringUtils.isEmpty(userField.getCity())) {
            error.append("field city's missing");
        }
        if (StringUtils.isEmpty(userField.getCountry())) {
            error.append("field country's missing");
        }
        if (StringUtils.isEmpty(userField.getEmail())) {
            error.append("field mail's empty");
        }
        if (StringUtils.isEmpty(userField.getPassword())) {
            error.append("field password's missing");
        }
        if (StringUtils.isEmpty(userField.getStreet())) {
            error.append("field street's missing");
        }
        String errorMsg = error.toString();
        //zbiera wszystkie stringi
        if (errorMsg.isEmpty() && !users.containsValue(userField.getUsername())) {
            users.put(userField.getUsername(), userField);
            return "registered: " + userField.getUsername();
        }
        return errorMsg;
    }

    @Override
    public boolean validate(LoginField login) {
        UserField user = users.get(login.getUsername());
        if (user.getPassword().equals(login.getPassword())) {
            return true;
        }
        return false;
    }

    @Override
    public UserField getUser(String login) {
        if (users.containsKey(login)){
         return users.get(login);
           }
                   return null;
    }
}
