package sda.spring.service;
import sda.spring.model.LoginField;
import sda.spring.model.UserField;

public interface UserDo {
    String register(UserField userField);
    boolean validate(LoginField login);
    UserField getUser(String login);

}
