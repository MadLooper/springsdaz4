package sda.spring.service;

import org.springframework.stereotype.Service;

import sda.spring.model.UserField;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
@Service
public class LoggedUsers {
private Map<String, UserField> loggedUsers = new ConcurrentHashMap<>();


    public void userLogin(UserField user){
    this.loggedUsers.put(user.getUsername(),user);
}

    public void logoutUser (UserField user){
        this.loggedUsers.remove(user.getUsername());
    }

    public List<UserField> getLoggedInUsers(){
        return  new ArrayList(this.loggedUsers.values());
    }
}
//dlaczego wszedzie dodajemy this?
//czym jest CurrentHashMap
//co roby get LoggedInUsers?