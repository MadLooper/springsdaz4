package sda.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import sda.spring.model.UserField;
import sda.spring.service.LoggedUsers;
import sda.spring.service.UserDo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegistrationControlle {
    @Autowired
    public UserDo userService;
    @Autowired
    public LoggedUsers loggedUsers;

    @RequestMapping(value = "/register",method = RequestMethod.GET)
    public ModelAndView showRegister (HttpServletRequest request, HttpServletResponse response){
        ModelAndView mav = new ModelAndView("register");
        mav.addObject("user",new UserField());
        return mav;
    }
    @RequestMapping(value = "/registerProcess",method = RequestMethod.POST)
    public ModelAndView addUser (HttpServletRequest request,HttpServletResponse response,
                                 @ModelAttribute("user")UserField user){
        String errors = userService.register(user);
        if (StringUtils.isEmpty(errors)){
            loggedUsers.userLogin(user);
        return  new ModelAndView("welcome","user",user);
        }
        ModelAndView mav = new ModelAndView("register");
        mav.addObject("message",errors);
        return mav;
    }
}
