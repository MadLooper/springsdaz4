package sda.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import sda.spring.model.LoginField;
import sda.spring.model.UserField;
import sda.spring.service.LoggedUsers;
import sda.spring.service.UserDo;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class LoginControlle {
    @Autowired
    UserDo userService;
    @Autowired
    LoggedUsers loggedUsers;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("login", new LoginField());
        return mav;
    }
    @RequestMapping(value = "/loginProcess",method = RequestMethod.POST)
    public ModelAndView loginProcess (HttpServletRequest request,HttpServletResponse response,
        @ModelAttribute("login")LoginField login){
        ModelAndView mav = null;
        boolean loginSucessful = userService.validate(login);
      if (loginSucessful) {
          mav = new ModelAndView("welcome");
          UserField user = userService.getUser(login.getUsername());
          loggedUsers.userLogin(user);
          mav.addObject("user", user);
      }else {
          mav=new ModelAndView("login");
          mav.addObject("message","Username or password is wrong");
      }
      return mav;
    }
    @RequestMapping(value = "/logoutProcess",method = RequestMethod.POST)
    public ModelAndView logoutProcess (HttpServletRequest request,HttpServletResponse response,
                                       @ModelAttribute("user")UserField user){
        loggedUsers.logoutUser(user);
        return new ModelAndView("home");
    }
}